#!/bin/bash
# build_openrc.sh

# Source repo packages and functions
[ -e repo_packages ] && . repo_packages
. functions || exit 1

# Set the user
if [ "$(sed -n '/nous/ p' <<< "$USER")" ]; then
	sfname=archnous
else
	sfname=aaditya1234
fi

# Set the variables
TARGETS=('x86_64' 'i686')
BASEDIR=/tmp
BUILDDIR=$BASEDIR/packages-openrc-master
LOGDIR=/tmp/openrc-autobuild-$(date +%F_%T)
LOGFILE=$LOGDIR/log
REPO=openrc-eudev
REPODIR=$BASEDIR/$REPO
REPO_REMOTE=$REPO-sf
if [[ $sfname = archnous ]]; then
	REPODIR_REMOTE=$BASEDIR/$REPO_REMOTE
else
	REPODIR_REMOTE=$HOME/$REPO_REMOTE
fi
AURDOWNLOAD="yaourt -G"
MAKEPKGOPTS=(--noconfirm --skipinteg --skippgpcheck -sc)
LINE="=================================================================== "
SFREPO="frs.sourceforge.net:/home/frs/project/archopenrc/openrc-eudev/"
GHREPO="https://github.com/manjaro/packages-openrc"
PATCHDIR="$(pwd)/patches"
SYNC=0

# Source config
[ -f config_vars.sh ] && . config_vars.sh

# Parse commandline args
[[ $@ = *i686* ]] && TARGETS=('i686')
[[ $@ = *x86_64* ]] && TARGETS=('x86_64')
[[ $@ = *armv7h* ]] && TARGETS=('armv7h')
if [[ $@ = *pfkernel* ]]; then
    REPODIR=/home/Dropbox/Public
    REPO='pfkernel'
fi

# create requisite directories
for cpu in "${TARGETS[@]}"; do
    mkdir -p ${REPODIR}/${cpu}
done

mkdir -p "$LOGDIR"

log() {
    echo "$*"
    echo "$(date +%F_%T)" " $* " >> "$LOGFILE"
}

# Building process starts
log "$LINE"
log "Building for CPU(s): ${TARGETS[*]}"

unalias cp mv rm 2>/dev/null
rm -fr $BUILDDIR /tmp/master.zip

cd "$BASEDIR"
wget -c -t 0 -T 10 -w 10 $GHREPO/archive/master.zip
if [[ ! $? -eq 0 ]]; then
    log "Error fetching packages. Aborting."
    exit 1
fi
unzip master.zip

cd "$BUILDDIR"
if [[ ! $? -eq 0 ]]; then
    log "Can't find sources. Aborting"
    exit 1
fi

#rm -f $REPODIR/*/*pkg.tar.xz

echo $SFNAME

if [ ${SYNC} -eq 0 ]; then
    echo "Fetching remote repo from sourceforge for $sfname"
    echo "Saving to $REPODIR_REMOTE"
    rsync -auvPH --delete-after "${sfname}"@"${SFREPO}" "$REPODIR_REMOTE" || exit 1
fi

rm -fr README.md .git* .kate*

echo "Downloading AUR packages"
for package in "${extras[@]}" "${extras_any[@]}"; do
    $AURDOWNLOAD "$package"
done

log "Start building"

for package in "${arches[@]}" "${extras[@]}"; do
    echo "$LINE"
    echo "Entering $package"
    if check_ignore "$package"; then
        echo "$package ignored."
        continue
    fi
    cd "$BUILDDIR/$package"
    echo "Sourcing PKGBUILD"
    . PKGBUILD
    for cpu in "${TARGETS[@]}"; do
        check_package "$REPODIR_REMOTE" "$pkgname" "$pkgver" "$pkgrel" "$cpu" && continue # package already present
	if [ -f "${PATCHDIR}/${package}.patch" ]; then
		cp "${PATCHDIR}/${package}.patch" .
		patch < ${package}.patch
	fi
        echo "Building $package for $cpu"
        if [[ $cpu = i686 ]]; then
            [[ $package = lib32-eudev-systemdcompat ]] && continue
            linux32 makepkg "${MAKEPKGOPTS[@]}" --config=/etc/makepkg.conf."$cpu" 1>"$LOGFILE-$package-$cpu"-build 2>"$LOGFILE-$package-$cpu"-errors || log "Error building $package; see $LOGFILE-$package-$cpu-errors for details"
        else
            makepkg "${MAKEPKGOPTS[@]}" --config=/etc/makepkg.conf."$cpu" 1>"$LOGFILE-$package-$cpu"-build 2>"$LOGFILE-$package-$cpu"-errors || log "Error building $package; see $LOGFILE-$package-$cpu-errors for details"
        fi
#        mv -vf "$package"-[0-9]*-"$cpu".pkg.tar.xz "$REPODIR/$cpu/"
        mv -vf *-[0-9]*-"$cpu".pkg.tar.xz "$REPODIR/$cpu/"
    done
#    rm -fr package
done

for package in "${any[@]}" "${extras_any[@]}"; do
    echo "$LINE"
    echo "Entering $package"
    cd "$BUILDDIR/$package"
    if check_ignore "$package"; then
        echo "$package ignored."
        continue
    fi
    echo "Sourcing PKGBUILD"
    . PKGBUILD
    # Special check for service packages
    if [[ $package = openrc-* ]]; then
        check_package "$REPODIR_REMOTE" "$pkgname" "$pkgver" "$pkgrel" "$TARGETS" "service" && continue # package already present
    else
        check_package "$REPODIR_REMOTE" "$pkgname" "$pkgver" "$pkgrel" "$TARGETS" && continue # package already present
    fi
    echo "Building $package for any"
    arch=any
    makepkg "${MAKEPKGOPTS[@]}" 1>"$LOGFILE-$package-$arch-build" 2>"$LOGFILE-$package-$arch-errors" || log "Error building $package; see $LOGFILE-$package-$arch-errors for details"
    for cpu in "${TARGETS[@]}"; do
        cp -vf ./*-"$arch".pkg.tar.xz "$REPODIR/${cpu}/"
    done
    rm ./*-"$arch".pkg.tar.xz
done

rm -fr "$BUILDDIR"

log "$LINE"
log "Finished building - updating repo database..."

# Copy the built packages
for repo in "${TARGETS[@]}"; do
    flag=0
    mkdir -p $REPODIR_REMOTE/$repo
    #cp -vf $(vercmp $REPODIR/$repo/*.pkg.tar.xz) $REPODIR_REMOTE/$repo/ || continue  # repo state unchanged, nothing to do
    cp -vf "$REPODIR/$repo"/*.pkg.tar.xz "$REPODIR_REMOTE/$repo/" || flag=1  # repo state unchanged, nothing to do
    if [[ $flag -eq 1 ]]; then
        echo "Nothing to do"
        continue
    fi
    cd "$REPODIR_REMOTE/$repo"
    # remove systemd
    rm -f systemd-*.pkg.tar.xz
    # remove old versions
    echo "Trimming $REPODIR_REMOTE/$repo of old packages..."
    paccache -rv -k1 -c .
    # add packages to pacman db
    nice -n 20 repo-add "$REPO.db.tar.gz" ./*pkg.tar.xz
    rm -f "$REPO.db"
    cp -f "$REPO.db.tar.gz" "$REPO.db"
    if [ ${SYNC} -eq 0 ]; then
        log "Press <ENTER> to upload to $SFREPO/$repo"
        read
        rsync -auvLPH --delete-after "$REPODIR_REMOTE/$repo" "${sfname}"@"${SFREPO}"/
    fi
done
